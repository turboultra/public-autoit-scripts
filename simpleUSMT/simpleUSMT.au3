#RequireAdmin
DllCall("kernel32.dll", "int", "Wow64DisableWow64FsRedirection", "int", 1) ; This disables 32bit applications from being redirected to syswow64
#include <ButtonConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <MsgBoxConstants.au3>
#include <resources/Zip.au3>

FileInstall('G:\Work\Current Projects\simpleUSMT\resources\simpleUSMT.zip', @ScriptDir & '\simpleUSMT.zip', 1)

Global $usmtdir = @ScriptDir & '\simpleUSMT\'
Global $migappxml = ' /i:"' & $usmtdir & 'MigApp.xml"'
Global $migdocsxml = ' /i:"' & $usmtdir & 'MigDocs.xml"'
Global $migexcludexml = ' /i:"' & $usmtdir & 'exclude.xml"'

;Check and create USMT files.
If FileExists($usmtdir & "\scanstate.exe") = 0 Then
	_ProgressBar("Installing dependencies...")
	_Zip_UnzipAll(@ScriptDir & '\simpleUSMT.zip', @ScriptDir)
	ProgressOff()
EndIf

FileDelete("simpleUSMT.zip")

#Region ### START Koda GUI section ### Form=
$Form1 = GUICreate("Simple USMT", 475, 147, -1, -1)
$Deploy = GUICtrlCreateButton("Deploy", 264, 40, 163, 57)
GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
$Capture = GUICtrlCreateButton("Capture", 52, 40, 163, 57)
GUICtrlSetFont(-1, 12, 400, 0, "MS Sans Serif")
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit
    Case $Capture ;When the 'Capture' button is pressed do
      GUISetState(@SW_HIDE) ;Hide original gui
			Global $selectscandir = _FolderSelect("Select the destination folder for capture.")
			Global $scandir = $selectscandir & '\' & @computername
			DirCreate($scandir)
			If FileExists($scandir & "\capture.done") Then
			$overwrite = MsgBox(4, "Overwrite?", "There was a previous capture done to this folder." & @CRLF & "Would you like to overwrite the exsisting data?")
				If $overwrite = 7 Then
				_ProgressBar("Exiting...")
				Sleep(2000)
				ProgressOff()
				Exit
				EndIf
			EndIf
			_ProgressBar('Capturing to: "...\' & @computername & '"')
      RunWait('"' & $usmtdir & 'scanstate.exe" ' & '"' & $scandir & '"' & $migappxml & $migdocsxml & $migexcludexml & ' /l:"' & $scandir & '\capture.log" /ue:*\setup* /ue:*\admin* /c /o /localonly')
			FileWrite($scandir & "\capture.done", "Capture complete.")
      ProgressOff()
      MsgBox(0, "Capture Complete", "Capture Complete" & @CRLF & "Migration file saved to: " & $scandir)
      Exit
    Case $Deploy ;When the 'Deploy' button is pressed do
      GUISetState(@SW_HIDE) ;Hide original gui
      Global $loaddir = _FolderSelect("Select the source folder for deployment.")
			Do
			_ProgressBar('Waiting for capture to complete...')
			Sleep (3000)
			ProgressOff()
			Until FileExists($loaddir & "\capture.done")
			_ProgressBar('Deploying from:"' & $loaddir & '"')
			RunWait('"' & $usmtdir & 'loadstate.exe" ' & '"' & $loaddir & '"' & $migappxml & $migdocsxml & $migexcludexml & ' /l:"' & $loaddir & '\deploy.log" /ue:*\admin* /c')
			ProgressOff()
			MsgBox(0, "Deployment is complete.", "Deployment is complete.")
      Exit
	EndSwitch
WEnd

;=================================Functions=====================================
;===============================================================================

Func _ProgressBar($name)
  Progresson($name, $name)
  Progressset(99, "")
EndFunc

Func _FolderSelect($message)
    Local $sFileSelectFolder = FileSelectFolder($message, @ScriptDir, 2)
    If @error Then
        MsgBox($MB_SYSTEMMODAL, "", "No folder was selected.")
				_ProgressBar("Exiting...")
				Sleep(2000)
				ProgressOff()
				Exit
    Else
				Return $sFileSelectFolder
    EndIf
EndFunc
