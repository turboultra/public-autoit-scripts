### [simpleUSMT.au3](https://gitlab.com/turboultra/public-autoit-scripts/tree/master/simpleUSMT)
A script to simplify USMT. An easy CAPTURE and DEPLOY to save MIG files and then restore files to a new computer. By default uses MigApp.xml, MigDocs.xml, and a custom excludes.xml.  

Place USMT files from `C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\User State Migration Tool\amd64` into a folder called simpleUSMT or zip it up into `simpleUSMT.zip` relative to the script path. I use the pure AutoIt [_Zip UDF](https://www.autoitscript.com/forum/topic/73425-zipau3-udf-in-pure-autoit/) from torels_ to bundle everything up into a single executable so techs can take the simpleUSMT tool where ever they need it.
