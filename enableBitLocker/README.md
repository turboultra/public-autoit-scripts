# enableBitLocker.au3
Windows 10 script for enabling BitLocker. This was written as a lightly interactive application to be used in a self-service environment. The script notifies the user of each step and then reboots the computer. The script assumes that the TPM has already been enabled and keys are backed up to AD by group policy.

Notes:  
I've traditionally used the VB script 'EnableBitLocker.vbs' provided by TechNet: [BitLocker Sample Deployment Script](https://gallery.technet.microsoft.com/scriptcenter/780d167f-2d57-4eb7-bd18-84c5293d93e3) but it doesn't support/work Windows 10.
