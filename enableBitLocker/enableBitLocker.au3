#RequireAdmin ; Requires admin rights to run the script.
#include <MsgBoxConstants.au3>

; This disables 32bit applications from being redirected to syswow64
DllCall("kernel32.dll", "int", "Wow64DisableWow64FsRedirection", "int", 1)

; cmd variables
Global $recoverypw = 'manage-bde -protectors -add c: -RecoveryPassword'
Global $bitlockeron = 'manage-bde -on c:'

; Notify user
MsgBox(0,'BitLocker', 'BitLocker will now enable encryption on your computer.' & @CRLF & 'A reboot will be required. Please save your work.')
RunWait(@ComSpec & " /c " & $recoverypw)
RunWait(@ComSpec & " /c " & $bitlockeron)

; Notify user of pending reboot and reboot the computer.
_rebootPC()

Func _rebootPC()
; Uses a progress bar to notify the user of a pending restart.
  ProgressOn("Reboot required", "The computer will restart in 10 seconds.", "10 seconds" )
  $is = 10
  For $i = 10 To 100 Step 10
  $is = $is - 1
  If $is <> 1 Then
    ProgressSet ($i, $is & " seconds")
  Else
    ProgressSet($i, $is & " second")
  Endif
    Sleep(1000)
  Next
  ProgressSet(100,"Rebooting")
  Sleep(1000)
  ProgressOff()
  Shutdown(6)
EndFunc  ;_rebootPC
