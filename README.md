# Public AutoIt Scripts
This is just a collection of AutoIt scripts I have written over the years for tedious tasks. Most of them are developed for Windows workstation deployment, others are just silly "I wonder if I could..." projects. I'll try to keep things well commented and mention sources in the README of each script.

### [enableBitLocker.au3](https://gitlab.com/turboultra/public-autoit-scripts/tree/master/enableBitLocker)
Windows 10 script for enabling BitLocker with minimal user interaction. Assumes TPM is available and RecoveryPassword is backed up to ActiveDirectory by GroupPolicy.

### [simpleUSMT.au3](https://gitlab.com/turboultra/public-autoit-scripts/tree/master/simpleUSMT)
A script to simplify USMT. An easy CAPTURE and DEPLOY to save MIG files and then restore files to a new computer. By default uses MigApp.xml, MigDocs.xml, and a custom excludes.xml.